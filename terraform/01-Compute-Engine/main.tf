#provider is google cloud 

provider "google" {
  project = "playground-s-11-8e7a90ad"
  region  = "us-central1"
}

#setting up a remote backend to a gcs bucket. this need to be created before executing the script
terraform {
  backend "gcs" {
    bucket = "sxm-tf-state"
    prefix = "01"
  }
}

resource "google_compute_instance" "instance1" {

  name         = "test"
  machine_type = "e2-medium"
  zone         = "us-central1-a"

  tags = ["foo", "bar"]

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-9"
    }
  }
  metadata = {
    "enable-oslogin" = "true"
  }
  network_interface {
    subnetwork = "web-sub-net"

    access_config {
      // Ephemeral IP
    }
  }
}
