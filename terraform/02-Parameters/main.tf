#provider is google cloud 

provider "google" {
  project = "playground-s-11-8e7a90ad"
  region  = var.region
}

#setting up a remote backend to a gcs bucket. this need to be created before executing the script
terraform {
  backend "gcs" {
    bucket = "sxm-tf-state"
    prefix = "02"
  }
}

resource "google_compute_instance" "instance1" {

  name         = var.servername
  machine_type = var.instancetype
  zone         = var.zone

  tags = var.tags

  boot_disk {
    initialize_params {
      image = var.imagename
    }
  }
  network_interface {
    subnetwork = var.subnetwork

    access_config {
      // Ephemeral IP
    }
  }
}
