variable "region" {
  type    = string
  default = "us-central1"
}

variable "zone" {
  type    = string
  default = "us-central1-a"
}

variable "tags" {
  type    = list
  default = ["foo","bar"]
}

variable "network" {
  type    = string
  default = "devops-vpc"
}


variable "websubnetwork" {
  type    = string
  default = "web-sub-net"
}

variable "dbsubnetwork" {
  type    = string
  default = "db-sub-net"
}

variable "servername" {
  type    = string
  default = "server"
}

variable "instancetype" {
  type    = string
  default = "e2-small"
}


variable "imagename" {
  type    = string
  default = "debian-cloud/debian-9"
}

variable "project" {
  type = string
}

variable "websubnetwork-cidr" {
  type    = string
}

variable "dbsubnetwork-cidr" {
  type    = string
}