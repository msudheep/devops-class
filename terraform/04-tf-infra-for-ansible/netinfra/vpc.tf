
# creating a VPC for our demos

resource "google_compute_network" "devops-vpc" {
  name                    = "devops-vpc"
  auto_create_subnetworks = false
}


#creating a subnet for web servers

resource "google_compute_subnetwork" "websubnet" {
  name          = var.websubnetwork
  ip_cidr_range = var.websubnetwork-cidr
  region        = var.region
  network       = google_compute_network.devops-vpc.id

}

#creating a subnet for db servers

resource "google_compute_subnetwork" "dbsubnet" {
  name          = var.dbsubnetwork
  ip_cidr_range = var.dbsubnetwork-cidr
  region        = var.region
  network       = google_compute_network.devops-vpc.id

}

#firewall rules for webservers

resource "google_compute_firewall" "ingress-ssh" {
  name    = "ingress-ssh"
  network = google_compute_network.devops-vpc.id

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = ["22"]
  }

  source_ranges = ["0.0.0.0/0"]
  target_tags   = ["controller","dbserver", "webserver"]
}


resource "google_compute_firewall" "ingress-http" {
  name    = "ingress-http"
  network = google_compute_network.devops-vpc.id

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = ["80"]
  }

  source_ranges = ["0.0.0.0/0"]
  target_tags   = ["webserver"]
}


resource "google_compute_firewall" "ssh-controller" {
  name    = "ssh-controller"
  network = google_compute_network.devops-vpc.id

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = ["22"]
  }

  source_tags = ["controller"]
  target_tags = ["dbserver", "webserver"]
}

resource "google_compute_firewall" "web-to-db" {
  name    = "web-to-db"
  network = google_compute_network.devops-vpc.id

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = ["3306"]
  }

  source_tags = ["webserver"]
  target_tags = ["dbserver"]
}


resource "google_compute_router" "router" {
  name    = "devops-router"
  region  = var.region
  network = google_compute_network.devops-vpc.id
}

resource "google_compute_router_nat" "nat" {
  name                               = "devops-router-nat"
  router                             = google_compute_router.router.name
  region                             = google_compute_router.router.region
  nat_ip_allocate_option             = "AUTO_ONLY"
  source_subnetwork_ip_ranges_to_nat = "ALL_SUBNETWORKS_ALL_IP_RANGES"
  
}