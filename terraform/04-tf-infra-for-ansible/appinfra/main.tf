#provider is google cloud 

provider "google" {
  project = var.project
  region  = var.region
}

#setting up a remote backend to a gcs bucket. this need to be created before executing the script
terraform {
  backend "gcs" {
    bucket = "sxm1-tf-state"
    prefix = "app"
  }
}
