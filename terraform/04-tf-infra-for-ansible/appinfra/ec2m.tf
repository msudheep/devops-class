
data "google_client_openid_userinfo" "user" {
}


locals {
    key-file = "~/.ssh/id_rsa.pub"
    ssh-keys = "ansible:${file(local.key-file)}"
}

resource "google_os_login_ssh_public_key" "cache" {
  user =  data.google_client_openid_userinfo.user.email
  key = file("~/.ssh/id_rsa.pub")
}

module "webserver1" {
    source = "../modules/gce"
    servername = "webserver1"
    instancetype = var.instancetype
    zone=var.zone
    tags=["webserver"]
    subnetwork=var.websubnetwork
    ssh-key=local.ssh-keys
  
}
module "webserver2" {
    source = "../modules/gce"
    servername = "webserver2"
    instancetype = var.instancetype
    zone=var.zone
    tags=["webserver"]
    subnetwork=var.websubnetwork
    ssh-key=local.ssh-keys    
 }

module "dbserver1" {
    source = "../modules/gce"
    servername = "dbserver1"
    instancetype = var.instancetype
    zone=var.zone
    tags=["dbserver"]
    subnetwork=var.dbsubnetwork
    ssh-key=local.ssh-keys
}

module "controller" {
    source = "../modules/gce"
    servername = "controller"
    instancetype = var.instancetype
    zone=var.zone
    tags=["controller"]
    subnetwork=var.websubnetwork
    ssh-key=local.ssh-keys    
}




resource "google_compute_instance_group" "webservers" {
  name        = "terraform-webservers"
  description = "Terraform test instance group"
  zone=var.zone

  instances = [
    module.webserver2.instance_details_int[0].id,
    module.webserver1.instance_details_int[0].id
  ]

  named_port {
    name = "http"
    port = "80"
  }
}

resource "google_compute_backend_service" "webserver-backend" {
  name          = "webserver-backend"
  project      = var.project
  health_checks = [google_compute_http_health_check.webserver-backend-hc.id]
  backend {
      group=google_compute_instance_group.webservers.id
  }


}

resource "google_compute_http_health_check" "webserver-backend-hc" {
  name               = "health-check"
  project      = var.project
  request_path       = "/"
  check_interval_sec = 1
  timeout_sec        = 1
}


resource "google_compute_global_address" "webserver-lb-address" {
  project      = var.project
  name         = "webserver-lb-address"
  ip_version   = "IPV4"
  address_type = "EXTERNAL"
}



resource "google_compute_global_forwarding_rule" "webserver-lb-rule" {
  provider = google-beta
  project      = var.project
  name   = "webserver-forwarding-rule"
  port_range            = "80"
  target                = google_compute_target_http_proxy.default.id
  #subnetwork            = "web-sub-net"
  ip_address = google_compute_global_address.webserver-lb-address.address
}

resource "google_compute_target_http_proxy" "default" {
  provider = google-beta
  project      = var.project
  name    = "website-proxy"
  url_map = google_compute_url_map.default.id
}

resource "google_compute_url_map" "default" {
  provider = google-beta
  project      = var.project
  name            = "website-map"
  default_service = google_compute_backend_service.webserver-backend.id
}


