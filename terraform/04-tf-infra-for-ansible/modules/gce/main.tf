locals {
  startupscript=join(" && ",["apt-get install python3", "useradd -m -p sa3tHJ3/KuYvI -s /bin/bash ansible","usermod -aG sudo ansible"])
}

resource "google_compute_instance" "gceinstance" {
 count        = var.ext_ip_create ? 0 : 1
  name         = var.servername
  machine_type = var.instancetype
  zone         = var.zone

  tags = var.tags

  boot_disk {
    initialize_params {
      image = var.imagename
    }
  }
  network_interface {
    subnetwork = var.subnetwork
  }
  metadata = {
    ssh-keys = var.ssh-key
  }
  metadata_startup_script = local.startupscript
}

resource "google_compute_instance" "gceinstanceext" {
 count        = var.ext_ip_create ? 1 : 0
  name         = var.servername
  machine_type = var.instancetype
  zone         = var.zone

  tags = var.tags

  boot_disk {
    initialize_params {
      image = var.imagename
    }
  }
  network_interface {
    subnetwork = var.subnetwork

    access_config {}
  }
    metadata = {
    ssh-keys = var.ssh-key
  }
  metadata_startup_script = local.startupscript
}
