#provider is google cloud 

provider "google" {
  project = "playground-s-11-cb9a307f"
  region  = "us-central1"
}

#setting up a remote backend to a gcs bucket. this need to be created before executing the script
terraform {
  backend "gcs" {
    bucket = "sxm1-tf-state"
    prefix = "vpc"
  }
}

# creating a VPC for our demos

resource "google_compute_network" "devops-vpc" {
  name                    = "devops-vpc"
  auto_create_subnetworks = false
}


#creating a subnet for web servers

resource "google_compute_subnetwork" "websubnet" {
  name          = "web-sub-net"
  ip_cidr_range = "10.1.1.0/24"
  region        = "us-central1"
  network       = google_compute_network.devops-vpc.id

}

#creating a subnet for db servers

resource "google_compute_subnetwork" "dbsubnet" {
  name          = "db-sub-net"
  ip_cidr_range = "10.1.2.0/24"
  region        = "us-central1"
  network       = google_compute_network.devops-vpc.id

}

#firewall rules for webservers

resource "google_compute_firewall" "ingress-web" {
  name    = "ingress-web"
  network = google_compute_network.devops-vpc.id

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = ["80","22"]
  }

  source_ranges = ["0.0.0.0/0"]
  target_tags   = ["webserver"]
}




