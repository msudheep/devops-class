provider "google" {
  project = "playground-s-11-e56f6bc1"
  region  = "us-central1"
}

#setting up a remote backend to a gcs bucket. this need to be created before executing the script
terraform {
  backend "gcs" {
    bucket = "sxm1-tf-state"
    prefix = "03"
  }
}

locals {
  
}

module "newgce" {
    source = "./modules/gce"
    servername = "server1"
    instancetype = "e2-small"
    zone="us-central1-a"
    tags=["webserver"]
    subnetwork="web-sub-net"
  
}

module "newgce2" {
    source = "./modules/gce"
    servername = "server2"
    instancetype = "e2-small"
    zone="us-central1-a"
    tags=["dbserver"]
    subnetwork="db-sub-net"
 
  
}


output "out1" {
  value=module.newgce
}

output "out2" {
  value=module.newgce2
}
