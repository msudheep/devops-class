variable "region" {
  type    = string
  default = "us-central1"
}

variable "zone" {
  type    = string
  default = "us-central1-a"
}

variable "tags" {
  type    = list
  default = ["foo","bar"]
}

variable "network" {
  type    = string
  default = "devops-vpc"
}


variable "subnetwork" {
  type    = string
}

variable "servername" {
  type    = string
  default = "server"
}

variable "instancetype" {
  type    = string
  default = "e2-medium"
}


variable "imagename" {
  type    = string
  default = "debian-cloud/debian-9"
}

