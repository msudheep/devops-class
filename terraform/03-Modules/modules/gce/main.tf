
resource "google_compute_instance" "gceinstance" {
  name         = var.servername
  machine_type = var.instancetype
  zone         = var.zone

  tags = var.tags

  boot_disk {
    initialize_params {
      image = var.imagename
    }
  }
  network_interface {
    subnetwork = var.subnetwork
        access_config {}
  }
}

